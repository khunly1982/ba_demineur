﻿using System;

namespace DemineurBA
{
    class Program
    {
        static Case[,] champsDeMines;
        static int nbColonnes;
        static int nbLignes;
        static int nbMines;
        static int posX = 0;
        static int posY = 0;

        /// <summary>
        /// générateur de nombre aléatoire
        /// </summary>
        static Random random = new Random();

        static void Main(string[] args)
        {
            // positionne à une certaine position le curseur de la console
            //Console.SetCursorPosition(6, 7);
            //Console.WriteLine("Bonjour");

            // (int, bool, string) maVariable = (2, true, "papa");
            // les tuples sont des types composés de plusieurs types


            #region Initialisation
            nbLignes = DemanderUnNombre("Combien de lignes ?");
            nbColonnes = DemanderUnNombre("Combien de colonnes ?");
            champsDeMines = new Case[nbLignes, nbColonnes];
            int minesMin = nbLignes * nbColonnes / 10;
            int minesMax = nbLignes * nbColonnes / 4;
            nbMines = DemanderUnNombre($"Combien de mines ? min: {minesMin}, max: {minesMax}", minesMin, minesMax);
            RemplirChampsDeMines();
            bool perdu = false;
            bool gagne = false;
            #endregion

            while (!perdu && !gagne) // si on tombes sur une bombes ou si toutes les cases non minées sont visibles
            {
                AfficherChampsMines();
                //int i = DemanderUnNombre("Entrer la ligne de la case à découvrir", 0, nbLignes - 1);
                //int j = DemanderUnNombre("Entrer la colonne de la case à découvrir", 0, nbColonnes - 1);

                SeDeplacer();

                if (!DecouvrirCase(posY, posX))
                {
                    perdu = true;
                    AfficherChampsMines(true);
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Vous avez perdu");
                    Console.Beep();
                }
                if (TestGagner())
                {
                    gagne = true;
                    AfficherChampsMines(true);
                    Console.ForegroundColor = ConsoleColor.DarkGreen;
                    Console.WriteLine("Vous avez gagné");
                    Console.Beep();
                }
            }

        }

        /// <summary>
        /// Deplace le curseur à une position particulière
        /// </summary>
        private static void SeDeplacer()
        {
            Console.SetCursorPosition(posX * 2 + 2, posY + 2);
            ConsoleKey key = default;
            while (key != ConsoleKey.Spacebar)
            {
                key = Console.ReadKey(true).Key;
                switch (key)
                {
                    case ConsoleKey.UpArrow:
                        posY = (--posY + nbLignes) % nbLignes;
                        break;
                    case ConsoleKey.DownArrow:
                        posY = ++posY % nbLignes;
                        break;
                    case ConsoleKey.LeftArrow:
                        if (posX > 0)
                            posX--;
                        else
                            posX = nbColonnes - 1;
                        break;
                    case ConsoleKey.RightArrow:
                        if (posX < nbColonnes - 1)
                            posX++;
                        else
                            posX = 0;
                        break;
                }
                Console.SetCursorPosition(posX * 2 + 2, posY + 2); 
            }
        }

        /// <summary>
        /// Test pour verifier si le joueur a gagné ou non
        /// </summary>
        /// <returns></returns>
        private static bool TestGagner()
        {
            for (int i = 0; i < nbLignes; i++)
            {
                for (int j = 0; j < nbColonnes; j++)
                {
                    if (!champsDeMines[i, j].EstVisible && champsDeMines[i, j].Valeur != 9)
                        return false;
                }
            }
            return true;
        }

        /// <summary>
        /// fonction récursive qui permet de découvrir une ou plusieurs cases
        /// </summary>
        /// <param name="i"></param>
        /// <param name="j"></param>
        /// <returns></returns>
        private static bool DecouvrirCase(int i, int j)
        {
            champsDeMines[i, j].EstVisible = true;
            if(champsDeMines[i, j].Valeur == 0)
            {
                for (int dj = j - 1; dj <= j + 1; dj++) // pourcourir les colonnes adjacentes à la case 0
                {
                    for (int di = i - 1; di <= i + 1; di++) // pourcourir les lignes adjacentes à la case 0
                    {
                        // si on ne sort pas des bornes ET si case adjacente n'a pas déjà été découverte
                        if (dj >= 0 && dj < nbColonnes && di >= 0 && di < nbLignes && !champsDeMines[di, dj].EstVisible)
                        {
                            // Decouvrir case adjacente
                            DecouvrirCase(di, dj);
                        }
                    }
                }
            }
            else if (champsDeMines[i, j].Valeur == 9)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Afficher le tableau
        /// </summary>
        private static void AfficherChampsMines(bool all = false)
        {
            // efface le contenu de la console
            Console.Clear();
            for(int i = 0; i < nbLignes; i++)
            {
                for(int j = 0; j < nbColonnes; j++)
                {
                    Console.SetCursorPosition((j * 2) + 2, i + 2);

                    if (champsDeMines[i, j].EstVisible || all)
                    {
                        switch (champsDeMines[i, j].Valeur)
                        {
                            case 0:
                                Console.ResetColor();
                                Console.WriteLine("0");
                                break;
                            case 1:
                                Console.ForegroundColor = ConsoleColor.Blue;
                                Console.WriteLine("1");
                                break;
                            case 2:
                                Console.ForegroundColor = ConsoleColor.Green;
                                Console.WriteLine("2");
                                break;
                            case 3:
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("3");
                                break;
                            case 4:
                                Console.ForegroundColor = ConsoleColor.Yellow;
                                Console.WriteLine("4");
                                break;
                            case 5:
                                Console.ForegroundColor = ConsoleColor.Cyan;
                                Console.WriteLine("5");
                                break;
                            case 6:
                                Console.ForegroundColor = ConsoleColor.DarkBlue;
                                Console.WriteLine("6");
                                break;
                            case 7:
                                Console.ForegroundColor = ConsoleColor.DarkCyan;
                                Console.WriteLine("7");
                                break;
                            case 8:
                                Console.ForegroundColor = ConsoleColor.DarkYellow;
                                Console.WriteLine("8");
                                break;
                            case 9:
                                Console.ForegroundColor = ConsoleColor.DarkRed;
                                Console.WriteLine("♥");
                                break;
                        } 
                    }
                    else
                    {
                        Console.ResetColor();
                        Console.WriteLine("■");
                    }
 
                }
            }
        }

        /// <summary>
        /// Remplir le champs de mines de bombes et augmenter les cases adjacentes 
        /// </summary>
        private static void RemplirChampsDeMines()
        {
            for(int x=0; x < nbMines; x++)
            {
                int i, j;
                do
                {
                    i = random.Next(0, nbLignes);
                    j = random.Next(0, nbColonnes);
                } while (champsDeMines[i, j].Valeur == 9); // entourer de qq chose ctrl + k + s

                // ajout d'une mine
                champsDeMines[i, j].Valeur = 9; // la valeur sera attribué aux bombes 
                for(int dj = j-1; dj <= j+1; dj++) // pourcourir les colonnes adjacentes à la mine
                {
                    for (int di = i-1; di <= i+1; di++) // pourcourir les lignes adjacentes à la mine
                    {
                        // si on ne sort pas des bornes et si la case  n'est une mine
                        if(dj >= 0 && dj < nbColonnes && di >= 0 && di < nbLignes && champsDeMines[di, dj].Valeur != 9)
                        {
                            // on augmente le nombre mine d'une unité
                            champsDeMines[di, dj].Valeur++;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Afficher un message à l'écran
        /// Demande d'entrer une valeur numérique comprise entre une borne min et une borne max.
        /// </summary>
        /// <param name="message">Le message qui sera afficher</param>
        /// <param name="min">La borne min</param>
        /// <param name="max">La borne max</param>
        /// <returns></returns>
        private static int DemanderUnNombre(string message, int min = 1, int max = 100)
        {
            string reponse = null;
            int result;
            do
            {
                if(reponse == null)
                {
                    Console.WriteLine(message);
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("La valeur entrée n'est pas correcte");
                    Console.ResetColor();
                }
                reponse = Console.ReadLine();

            } while (!int.TryParse(reponse, out result) || !(result >= min && result <= max));
            return result;
        }
    }
}

// !a && !b <=> !(a || b)
// !a || !b <=> !(a && b)
