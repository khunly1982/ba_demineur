﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemineurBA
{
    struct Case
    {
        public int Valeur;

        public bool EstVisible;

        //// pour générer le constructeur selectionner les variables -> alt + enter -> enter 
        public Case(int valeur, bool estVisible)
        {
            Valeur = valeur;
            EstVisible = estVisible;
        }
    }
}
